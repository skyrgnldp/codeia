import React, { Component } from 'react';
import { Switch, Router, Route } from 'react-router-dom';
import history from './js/history';


import Error404 from './components/Error404';
import Store from './components/marketPlace/Store';
import MarketPlace from './components/marketPlace/MarketBody';
import Cart from './components/marketPlace/Cart';
import Register from './components/authentication/Register';
import ForgotPassword from './components/authentication/ForgotPassword';
import ItemDetails from './components/marketPlace/ItemDetails';
import Payment from './components/marketPlace/Payment';

import './App.css';
import './css/commons.css';
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router history={history}>
          <Switch>
            <MarketPlace>
              <Route exact path="/" component={Store} />
              <Route path="/cart" component={Cart} />
              <Route path="/register" component={Register} />
              <Route path="/forgotPassword" component={ForgotPassword} />
              <Route path="/itemDetails" component={ItemDetails} />
              <Route path="/payment" component={Payment} />
            </MarketPlace>
            <Route path="*" component={Error404} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
