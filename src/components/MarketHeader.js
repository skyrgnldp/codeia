import React, {Component} from 'react';
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Modal,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

import jwtDecode from 'jwt-decode';

import LoginModal from './authentication/Login';
import history from '../js/history';
import Config from '../js/config';
import { createApolloFetch } from 'apollo-fetch';

import '../css/MarketHeader.css';


let uri = Config.api;
let apolloFetch = createApolloFetch({ uri });

class MarketHeader extends Component {

  constructor(props){
    super(props);
    this.state = {
      loginModal: false,
      mobileViewIsOpen: false,
      isLoggedInViaFacebook: false,
      isLoggedInViaNormalLogin: false,
      facebookAuthData: {},
      normalLoginInfo: {},
      profileDropdownOpen: false
    }
  }

  async componentWillMount(){

    if(localStorage.getItem('AuthToken')){
      let decoded = await jwtDecode(localStorage.getItem('AuthToken'));
      this.setState({
        normalLoginInfo: decoded.data,
        isLoggedInViaNormalLogin: true
      });
      console.log(this.state.normalLoginInfo);
    }
  }

  async componentDidMount(){
    if(localStorage.getItem('FacebookAuthSession')){
      let token = localStorage.getItem('FacebookAuthSession');
      let decoded = await jwtDecode(token);
      this.setState({
        isLoggedInViaFacebook: true,
        facebookAuthData: decoded.data
      });
      console.log(this.state.facebookAuthData);
    }
  }


  mobileViewToggle = () => {
    this.setState({
      mobileViewIsOpen: !this.state.mobileViewIsOpen
    });
  }

  loginModalToggle = async () => {
    await this.setState({
      loginModal: !this.state.loginModal
    });
  }

  loginModalToggleFromHeader = async (e) => {
    await this.setState({
      loginModal: e
    });
  }

  profileDropdownToggle = () => {
   this.setState({
     profileDropdownOpen: !this.state.profileDropdownOpen
   });
 }

 logout = () => {
   localStorage.clear();
   window.location.reload();
 }

 brandLogoClick = () => {
   window.location.href = "https://codeia.net";
 }

  render(){

    let headerDisplay;
    // delete the ! after
    if(this.state.isLoggedInViaFacebook){
      headerDisplay =
      <div className="loggedInHeader">
        <span>
          <div className="cartLogo" onClick={() => history.push('/cart')}>
            <img src={require('../images/market/icon-cart.svg')} alt=""/>
            <label>1</label>
          </div>
          <Dropdown className="profileDropdown" isOpen={this.state.profileDropdownOpen} toggle={this.profileDropdownToggle}>
            <DropdownToggle className="profileDropdownButton" caret>
              {/* <img className="fbProfileImage" src={this.state.facebookAuthData.avatar} alt=""/> */}
              {this.state.facebookAuthData.firstname}
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem>Profile</DropdownItem>
              <DropdownItem>Settings</DropdownItem>
              <DropdownItem divider />
              <DropdownItem onClick={this.logout}>Logout</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </span>
      </div>
    }else if(this.state.isLoggedInViaNormalLogin){
      headerDisplay =
      <div className="loggedInHeader">
        <span>
          <div className="cartLogo" onClick={() => history.push('/cart')}>
            <img src={require('../images/market/icon-cart.svg')} alt=""/>
            <label>1</label>
          </div>
          <Dropdown className="profileDropdown" isOpen={this.state.profileDropdownOpen} toggle={this.profileDropdownToggle}>
            <DropdownToggle className="profileDropdownButton" caret>
              {/* <img src="" alt=""/> */}
              {this.state.normalLoginInfo.firstname + ' ' + this.state.normalLoginInfo.lastname}
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem>Profile</DropdownItem>
              <DropdownItem>Settings</DropdownItem>
              <DropdownItem divider />
              <DropdownItem onClick={this.logout}>Logout</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </span>
      </div>
    }

    else {
      headerDisplay =
      <div className="headerDisplayBody">
        <div className="cartLogo" onClick={() => history.push('/cart')}>
          <img src={require('../images/market/icon-cart.svg')} alt=""/>
          <label>1</label>
        </div>
        <span onClick={this.loginModalToggle}>Login</span>
        <label className="slash" disabled>/</label>
        <span onClick={() => history.push('/register')}>Sign up</span>
      </div>
    }



    return (
      <div className="marketHeader">
        <Navbar expand="md">
          <Container>
            <NavbarBrand onClick={this.brandLogoClick}><img src={require('../images/market/logo-CodeiaMktPlace.svg')} alt=""/></NavbarBrand>
            <NavbarToggler className={this.state.mobileViewIsOpen === true ? 'mobileViewActive' : ''} onClick={this.mobileViewToggle} />
            <Collapse className="marketHeaderCollapse" isOpen={this.state.mobileViewIsOpen} navbar>
              <Nav className="linksBody ml-auto" navbar>
                {headerDisplay}
              </Nav>
            </Collapse>
            <Modal className="loginModalContent" isOpen={this.state.loginModal} size="md">
              <LoginModal toggleLoginModal={this.loginModalToggleFromHeader} />
            </Modal>
          </Container>
        </Navbar>
      </div>
    );
  }
}
export default MarketHeader;
