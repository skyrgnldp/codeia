import React, {Component} from 'react';
import { Container, Row, Col, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

import '../../css/marketPlace/Payment.css';

class Payment extends Component {
  constructor(props){
    super(props);
    this.state = {
      totalPrice: 15.99,
      countrySelected: 'Select Country *',
      regionSelected: 'Select Region *',
      regionsShown: [],
      dropdownCountry: false,
      dropdownRegion: false
    }
  }

  toggleDropdownCountry = () => {
    this.setState({
      dropdownCountry: !this.state.dropdownCountry
    });
  }
  toggleDropdownRegion = () => {
    if(this.state.countrySelected === 'Select Country *'){

    }else {
      this.setState({
        dropdownRegion: !this.state.dropdownRegion
      });

    }
  }

  selectCountry = async (e) => {
    await this.setState({
      countrySelected: e.country,
      regionsShown: e.regions
    });
  }
  selectRegion = async (e) => {
    await this.setState({
      regionSelected: e,
    });
  }

  paymentOnChange = async e => {
    await this.setState({
      [e.target.name]: e.target.value
    });
  }



  render(){

  const ordersData = [
    {name: 'Admin Template', price: '14'},
    {name: 'Install your config', price: '9.99'},
  ];

  const countriesAndRegions = [
    {
      country: 'Philippines',
      regions: [
        'Region IV-A',
        'Calabarzon',
        'MIMAROPA'
      ]
    },
    {
      country: 'China',
      regions: [
        'Guangxi',
        'Inner Mongolia',
        'Tibet',
        'Ningxia',
        'Xinjiang'
      ]
    },
    {
      country: 'Singapore',
      regions: [
        'Central Region',
        'North Region',
        'North-East Region',
        'East Region'
      ]
    },
    {
      country: 'Japan',
      regions: [
        'Hokaido',
        'Kansai',
        'Shikoku'
      ]
    },
  ];




    return (
      <div className="payment">
        <Container>
          <Row className="paymentContent">
            <Col lg="3" className="orders">
              <h4>Order Summary</h4>
              <ul>
                {
                  ordersData.map(( item, index ) => (
                    <li key={index}>
                      <p>{item.name}</p>
                      <p>${parseFloat(item.price).toFixed(2)}</p>
                    </li>
                  ))
                }
              </ul>
              <hr/>
              <span>
                <h5>Purchase Total: </h5>
                <h5>${parseFloat(this.state.totalPrice).toFixed(2)} </h5>
              </span>
              <button>Pay Now</button>
            </Col>
            <Col lg="9" className="right">
              <div className="forms">
                <label>Payment Method</label>
                <hr/>
                <div className="inputs">
                  <input type="text"
                         placeholder="First name *"
                         name="firstName"
                         onChange={this.paymentOnChange}
                  />
                  <input type="text"
                         placeholder="Last name *"
                         name="lastName"
                         onChange={this.paymentOnChange}
                  />
                  <input type="text"
                         placeholder="Card Number *"
                         name="cardNumber"
                         onChange={this.paymentOnChange}
                  />
                  <Row>
                    <Col md="5">
                      <input type="text"
                             placeholder="CSC Code *"
                             name="cscCode"
                             onChange={this.paymentOnChange}
                      />
                    </Col>
                    <Col md="5">
                      <input type="text"
                             placeholder="Month *"
                             maxLength="15"
                             name="month"
                             onChange={this.paymentOnChange}
                      />
                    </Col>
                    <Col md="2">
                      <input type="text"
                             placeholder="Year *"
                             maxLength="4"
                             name="year"
                             onChange={this.paymentOnChange}
                      />
                    </Col>
                  </Row>
                  <input type="text"
                         placeholder="Street Address *"
                         maxLength="4"
                         name="streetAddress"
                         onChange={this.paymentOnChange}
                  />
                  <input type="text"
                         placeholder="City *"
                         maxLength="4"
                         name="city"
                         onChange={this.paymentOnChange}
                  />
                  <Row>
                    <Col md="5">
                      <Dropdown className="formDropdown" isOpen={this.state.dropdownCountry} toggle={this.toggleDropdownCountry}>
                        <DropdownToggle className="formDropdownButton" caret>
                          {this.state.countrySelected}
                        </DropdownToggle>
                        <DropdownMenu className="formDropdownMenu">
                          {
                            countriesAndRegions.map(( item, index ) => (
                              <DropdownItem key={index}
                                            className="formDropdownItem"
                                            onClick={() => this.selectCountry(item)}>
                                {item.country}
                              </DropdownItem>
                            ))
                          }
                        </DropdownMenu>
                      </Dropdown>
                    </Col>
                    <Col md="5">
                      <Dropdown className="formDropdown" isOpen={this.state.dropdownRegion} toggle={this.toggleDropdownRegion}>
                        <DropdownToggle className="formDropdownButton" caret>
                          {this.state.regionSelected}
                        </DropdownToggle>
                        <DropdownMenu className="formDropdownMenu">
                          {
                            this.state.regionsShown.map(( item, index ) => (
                              <DropdownItem key={index}
                                            className="formDropdownItem"
                                            onClick={() => this.selectRegion(item)}>
                                {item}
                              </DropdownItem>
                            ))
                          }
                        </DropdownMenu>
                      </Dropdown>
                    </Col>
                    <Col md="2">
                      <input type="text"
                             placeholder="Year *"
                             maxLength="4"
                             name="year"
                             onChange={this.paymentOnChange}
                      />
                    </Col>
                  </Row>
                </div>
                <div className="terms">
                  <p>
                    By clicking "Process Payment", you agree to Mojo's <b>Terms of Service</b> and <b>Cancellation Policy</b> and acknowledge receipt of our <b>Privacy Policy</b>.
                  </p>
                  <p>You also agree to have your personal information transferred and stored in the United States, which is necessary to provide you with the services under our agreement with you.</p>
                  <p>All Theme support automatically renews unless you cancel. The renewal will be for the same term length and at the regular rates reflected in the My Purchases section of your Marketplace account. The payment method you provide today, or that we have on file, will be used for renewals, unless you change it or cancel. To cancel please call customer support at 855-464-5345.</p>
                </div>
                <div className="processPayment">
                  <button>Process Payment</button>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
export default Payment;
