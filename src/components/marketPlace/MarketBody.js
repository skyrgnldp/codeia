import React, {Component} from 'react';
import MarketHeader from '../MarketHeader';
import Menu from './Menu';
import Banner from './Banner';
import Footer from '../Footer';

import '../../css/marketPlace/MarketBody.css';

class MarketBody extends Component {
  constructor(props){
    super(props);
    this.state = {
      bannerTitle: '',
      bannerSubtitle: '',
    }
  }

  componentWillMount(){
    if(this.props.location.pathname === "/"){
      this.setState({
        bannerTitle: 'Themes & Templates',
        bannerSubtitle: '1988 Items found'
      });
    }else if(this.props.location.pathname === "/register"){
      this.setState({
        bannerTitle: 'Account Creation',
        bannerSubtitle: 'Register and join our growing community for FREE!'
      });
    }else if(this.props.location.pathname === "/forgotPassword"){
      this.setState({
        bannerTitle: 'Account password reset',
        bannerSubtitle: 'Password Reset'
      });
    }else if(this.props.location.pathname === "/itemDetails"){
      this.setState({
        bannerTitle: 'Item Details',
        bannerSubtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      });
    }else if(this.props.location.pathname === "/cart"){
      this.setState({
        bannerTitle: 'Shopping Cart',
        bannerSubtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      });
    }else if(this.props.location.pathname === "/payment"){
      this.setState({
        bannerTitle: 'Payment Method',
        bannerSubtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      });
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.location.pathname === "/register"){
      this.setState({
        bannerTitle: 'Account Creation',
        bannerSubtitle: 'Register and join our growing community for FREE!'
      });
    }else if(nextProps.location.pathname === "/"){
      this.setState({
        bannerTitle: 'Themes & Templates',
        bannerSubtitle: '1988 Items found'
      });
    }else if(nextProps.location.pathname === "/forgotPassword"){
      this.setState({
        bannerTitle: 'Account password reset',
        bannerSubtitle: 'Password Reset'
      });
    }else if(nextProps.location.pathname === "/itemDetails"){
      this.setState({
        bannerTitle: 'Item Details',
        bannerSubtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      });
    }else if(nextProps.location.pathname === "/cart"){
      this.setState({
        bannerTitle: 'Shopping Cart',
        bannerSubtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      });
    }else if(nextProps.location.pathname === "/payment"){
      this.setState({
        bannerTitle: 'Payment Method',
        bannerSubtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      });
    }
    window.scrollTo(0, 0);
  }

  render(){
    return (
      <div className="marketBody">
        <MarketHeader />
        <Menu />
        <Banner bannerTitle={this.state.bannerTitle} bannerSubtitle={this.state.bannerSubtitle} />
          <div className="marketBodyContent">
            {this.props.children}
          </div>
        <Footer />
      </div>
    );
  }
}
export default MarketBody;
